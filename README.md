# BoxScout

This is a simple app that queries various shipping companies for parcel info in
the command-line. It mostly just gives some information about the shipment and its
current location, as available.

**Warning** Do not use this in any serious capacity, since some shipping companies
have exclusive APIs that should be used instead.

# Examples

## Querying

To see all the shipping companies that are currently supported use

```sh
boxscout --help
```

Then each individual command can be accessed through e.g.

```sh
boxscout dpd <AWB>
```
where AWB is whatever parcel number or air waybill number supported by the
company. This should show some basic information about the status and history
of the parcel, e.g.

```
WEB TRACKING
    URL: https://tracking.dpd.de/status/en_RO/parcel/<AWB>
SHIPMENT INFO
    Service: DPD
    Parcel number: <AWB>
    Type: Deferred, B2C
SHIPMENT STATUS
    (02.01.2024 18:21) [DPD data centre] Order information has been transmitted to DPD.
    (04.01.2024 14:18) [LOCATION 1] In transit.
    (05.01.2024 14:08) [LOCATION 2] In transit.
    (09.01.2024 05:18) [LOCATION 3] At parcel delivery centre.
    (09.01.2024 08:10) [LOCATION 3] Out for delivery.
    (09.01.2024 10:30) [LOCATION 3] Delivered.
```

Some commands require additional authentication, since no open and freely available
APIs are available.

## History Management

The command also has some very basic history management. You can add an AWB to
the database using
```sh
boxscout manage --interactive
# or
boxscout manage --provider '<name>' --note '<some note about the parcel>' AWB
```
which can then be visualized using
```sh
boxscout history
```
Then, when trying to look up a previous AWB for a given provider, just use
```sh
boxscout PROVIDER
```
which will pop up a list of known AWBs to choose from. This is a very basic
interface, which will hopefully be improved in the future.

# Configuration

The configuration file is located at `XDG_CONFIG_HOME/boxscout/boxscout.toml`.
Each command (provider) that requires configuration uses entries of the form

```toml
[<PROVIDER>]
option = "DEFAULT_VALUE"

[<PROVIDER>.<OTHER_USER>]
option = "OVERWRITTEN_VALUE"
```

## ECONT

You need to sign up for the service (which is free) and save the login information
in a configuration file

```toml
[econt]
username = "<user@example.com>"
password = "<password>"
# or (recommended)
password_cmd = 'gopass show -o econt.com/<username@example.com>'
```

The `password_cmd` entry can contain any command that retrieves the password
from a more secure location. This example uses the nifty [gopass](https://github.com/gopasspw/gopass)
password manager. It is highly recommended to use a password command like this,
so that the cleartext `password` string is not directly stored in the configuration
file.

The format above gives the default options and additional users can be
added.

## FAN Courier

For this command, you need access to the FAN Courier API with an official
account. The configuration should contain

```toml
[fancourier]
username = "<username>"
client_id = "<client_id>"
password = "<password>"
# or (recommended)
password_cmd = "gopass show -o fancourier.ro/<username>"
```

Additional users can also be added in a similar fashion.

# License

The code is licensed under the MIT license (see `LICENSES/MIT.txt`).
