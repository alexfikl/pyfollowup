# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime

import pytest


def test_display() -> None:
    from boxscout import display

    d = datetime.now()
    sc0 = display.Section(
        name="Status",
        children=(
            display.Event(code=0, date=d, location="Tokyo", descr="Left train station"),
            display.Event(code=0, date=d, location="Tokyo", descr="Got to restaurant"),
            display.Event(code=0, date=d, location="Tokyo", descr="Ate some sushi"),
            display.Event(code=1, date=d, location="Tokyo", descr="Had to leave"),
        ),
    )

    sc1 = display.Section(
        name="Info",
        children=(
            display.Info(name="Price", descr="$42.0"),
            display.Info(name="Happiness", descr="Priceless"),
        ),
    )

    display.display([sc0, sc1])
    result = display.stringify([sc0, sc1], color=False)
    assert result

    result = display.stringify(sc1.children[0])
    assert result == "Price: $42.0", repr(result)

    with pytest.raises(TypeError):
        display.display(["error"])  # type: ignore[list-item]


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
