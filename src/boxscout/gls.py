# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime
from typing import Any

from boxscout import display
from boxscout.misc import ParseFailedError, clean_string, make_logger

logger = make_logger(__name__)

GLS_URL = "https://gls-group.eu/RO/en/parcel-tracking.html?match={awb}"
GLS_API_URL = "https://gls-group.eu/app/service/open/rest/RO/en/rstt001?match={awb}"


def get_metadata(awb: str) -> dict[str, Any]:
    from boxscout.misc import get_json_metadata

    api = GLS_API_URL.format(awb=awb)
    return get_json_metadata(api)


def parse_metadata(metadata: dict[str, Any]) -> tuple[str, display.ParseResult]:
    result = metadata.get("tuStatus")
    if result is None:
        raise ParseFailedError("Response has unexpected format")

    # get back the AWB
    awb = result[0]["referenceNo"]

    # web tracking
    url = GLS_URL.format(awb=awb)
    web = display.Section(
        name="Web Tracking",
        children=(display.Info("URL", url),),
    )

    # info
    info = display.Section(
        name="Shipment Info",
        children=(
            display.Info("Service", "GLS"),
            display.Info("Parcel number", awb),
        ),
    )

    # status
    children = []
    for event in result[0]["history"]:
        date = datetime.strptime(
            "{} {}".format(event["date"], event["time"]), "%Y-%m-%d %H:%M:%S"
        )
        location = event["address"]["city"]
        descr = event["evtDscr"].strip(".")
        children.append(
            display.Event(
                code=0,
                date=date,
                location=clean_string(location),
                descr=clean_string(descr),
            )
        )
    status = display.Section(name="Shipment Status", children=tuple(children))

    return url, display.ParseResult(web, info, status)
