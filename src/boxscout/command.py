# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime
from importlib import metadata
from typing import Any

import click

from boxscout import display
from boxscout.misc import (
    PROVIDER_DISPLAY_NAME,
    MetadataFailedError,
    ParseFailedError,
    make_logger,
)

logger = make_logger(__name__)


# {{{ utils


def clipboard(text: str) -> None:
    try:
        import pyperclip

        pyperclip.copy(text)
        logger.info("Copied to clipboard.")
    except ImportError:
        logger.error("Copy to clipboard: 'pyperclip' not available!")


# }}}


# {{{ commands


@click.group(
    context_settings={"show_default": True, "max_content_width": 100},
    invoke_without_command=False,
)
@click.help_option("--help", "-h")
@click.version_option(version=metadata.version("boxscout"))
@click.option(
    "-c",
    "--clip",
    is_flag=True,
    default=False,
    help="Copy the web tracking URL to the clipboard",
)
@click.option(
    "-q",
    "--quiet",
    is_flag=True,
    default=False,
    help="Only show error messages",
)
@click.pass_context
def main(
    ctx: click.Context,
    clip: bool,  # noqa: FBT001
    quiet: bool,  # noqa: FBT001
) -> None:
    if quiet:
        import logging

        # NOTE: set logging level on root logger only since it propagates
        root = logging.getLogger("boxscout")
        root.setLevel(logging.ERROR)

    ctx.ensure_object(dict)
    ctx.obj["clip"] = clip


# }}}


# {{{ manage


def format_note(provider: str, note: str | None) -> str:
    today = datetime.today().strftime("%d.%m.%Y at %H:%M")
    if note:
        note = note.format(
            PROVIDER=PROVIDER_DISPLAY_NAME[provider],
            TODAY=datetime.today().strftime("%d.%m.%Y"),
            TODAY_TIME=today,
        ).strip()
    else:
        note = f"Parcel from {PROVIDER_DISPLAY_NAME[provider]} added on {today}"

    return note


@main.command("manage")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.option(
    "-p",
    "--provider",
    default=None,
    type=click.Choice(list(PROVIDER_DISPLAY_NAME), case_sensitive=False),
    help="A provider for the given AWB",
)
@click.option(
    "-n",
    "--note",
    default=None,
    help="A note to add with the AWB (uses Python formatting)",
)
@click.option(
    "-d/-D",
    "--delivered/--no-delivered",
    is_flag=True,
    default=None,
    help="Mark the AWB as delivered",
)
@click.option(
    "-i",
    "--interactive",
    is_flag=True,
    default=False,
    help="Add AWB interactively",
)
@click.option(
    "-f",
    "--force",
    is_flag=True,
    default=False,
    help="Update AWB metadata even if it already exists",
)
@click.pass_context
def cli_manage(
    ctx: click.Context,
    awb: str,
    provider: str | None,
    note: str | None,
    delivered: bool | None,  # noqa: FBT001
    interactive: bool,  # noqa: FBT001
    force: bool,  # noqa: FBT001
) -> None:
    """Managed stored AWB information from various trackers."""
    from boxscout.misc import history_retrieve, history_save, history_search

    providers = history_retrieve()

    if interactive or not awb:
        awb = click.prompt(
            click.style("AWB", bold=True),
            default=awb if awb else None,
            show_default=True,
        )
        provider = click.prompt(
            click.style("Provider", bold=True),
            default=provider if provider else history_search(providers, awb),
            type=click.Choice(list(PROVIDER_DISPLAY_NAME), case_sensitive=False),
            show_choices=True,
        )
        provider = str(provider)

        note = click.prompt(click.style("Note", bold=True), type=str, default=note)
        delivered = click.prompt(
            click.style("Delivered (y/n)", bold=True),
            default=delivered,
            type=click.BOOL,
        )

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    if not provider:
        provider = history_search(providers, awb)

    if not provider:
        logger.error("Must choose a provider for AWB '%s'.", awb)
        ctx.exit(1)

    provider = provider.lower()

    awbs = providers.setdefault(provider, {})
    m = awbs.setdefault(awb, {})
    if m:
        if not force:
            logger.error(
                "Given AWB already exists for provider '%s'. Use '--force' to "
                "update values.",
                provider,
            )
            return

        if note:
            m["note"] = format_note(provider, note)

        if delivered is not None:
            m["delivered"] = delivered
        logger.info("Updated AWB: '%s'.", awb)
    else:
        m["created"] = datetime.today().isoformat()
        m["delivered"] = bool(delivered)
        m["note"] = format_note(provider, note)
        logger.info("Added new AWB: '%s'.", awb)

    history_save(providers)


@main.command("drop")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.option(
    "-p",
    "--provider",
    default=None,
    type=click.Choice(list(PROVIDER_DISPLAY_NAME), case_sensitive=False),
    help="A provider for the given AWB",
)
def cli_drop(ctx: click.Context, awb: str, provider: str | None) -> None:
    from boxscout.misc import history_retrieve, history_save, history_search

    if not awb:
        logger.error("AWB not provided.")
        ctx.exit(1)

    providers = history_retrieve()
    if provider is None:
        provider = history_search(providers, awb)

    if not provider:
        logger.error("Provider not provided for AWB: '%s'.", awb)
        return

    awbs = providers.get(provider)
    if not awbs:
        return

    if awb not in awbs:
        return

    del awbs[awb]
    logger.info("Removing AWB '%s'.", awb)

    history_save(providers)


# }}}


# {{{ history


def history_display(
    providers: dict[str, dict[str, Any]],
) -> tuple[display.Section[display.Node], ...]:
    if not providers:
        return (display.Section("No providers", children=()),)

    return tuple(
        display.Section(
            name=PROVIDER_DISPLAY_NAME[provider],
            children=tuple(
                display.History(awb=awb, note=m["note"], delivered=m["delivered"])
                for awb, m in sorted(awbs.items(), key=lambda x: x[1]["delivered"])
            )
            or (display.Info(PROVIDER_DISPLAY_NAME[provider], "No AWBs available."),),
        )
        for provider, awbs in providers.items()
    )


@main.command("history")
@click.help_option("--help", "-h")
@click.argument(
    "provider",
    default="",
    type=click.Choice([*PROVIDER_DISPLAY_NAME, ""], case_sensitive=False),
)
@click.option(
    "--clear",
    is_flag=True,
    default=False,
    help="Clear all cached information",
)
@click.pass_context
def cli_history(
    ctx: click.Context,
    provider: str,
    clear: bool,  # noqa: FBT001
) -> None:
    """Display all known AWBs for a given provider."""
    from boxscout.misc import history_clear, history_retrieve

    if clear:
        history_clear()
        return

    providers = history_retrieve()
    if provider:
        display.display(history_display({provider: providers.get(provider, {})}))
    else:
        display.display(history_display(providers))


# }}}


# {{{ providers


def awb_prompt(provider: str) -> str:
    from boxscout.misc import history_retrieve

    assert provider in PROVIDER_DISPLAY_NAME
    awbs = sorted(
        history_retrieve().get(provider, {}).items(),
        key=lambda x: x[1]["delivered"],
    )
    if not awbs:
        return ""

    display.display(
        display.Section(
            name=PROVIDER_DISPLAY_NAME[provider],
            children=tuple(
                display.History(
                    awb=f"({i}) {awb}",
                    note=m["note"],
                    delivered=m["delivered"],
                )
                for i, (awb, m) in enumerate(awbs)
            ),
        )
    )
    n = len(awbs)
    choice = click.prompt(
        f"Choose AWB (0 - {n - 1})",
        default=0,
        type=click.IntRange(0, n, max_open=True),
        show_choices=True,
    )
    return str(awbs[choice][0])


@main.command("gls")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.pass_context
def cli_gls(
    ctx: click.Context,
    awb: str,
) -> None:
    """Retrieve tracking information from GLS <https://gls-group.eu>.

    Given an AWB, we can retrieve basic information about a parcel's history
    and its current status. For additional information, check the provided URL.
    """
    from boxscout import gls

    if not awb:
        awb = awb_prompt("gls")

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    try:
        m = gls.get_metadata(awb)
    except MetadataFailedError as exc:
        logger.error("Failed to retrieve metadata: %s.", exc)
        ctx.exit(1)

    try:
        url, result = gls.parse_metadata(m)
    except ParseFailedError as exc:
        logger.error("Failed to parse metadata: %s.", exc)
        ctx.exit(1)

    display.display(result)

    if ctx.obj["clip"]:
        clipboard(url)


@main.command("dpd")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.pass_context
def cli_dpd(
    ctx: click.Context,
    awb: str,
) -> None:
    """Retrieve tracking information from DPD <https://dpd.ro>.

    Given an AWB, we can retrieve basic information about a parcel's history
    and its current status. For additional information, check the provided URL.
    """
    from boxscout import dpd

    if not awb:
        awb = awb_prompt("dpd")

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    try:
        m = dpd.get_metadata(awb)
    except MetadataFailedError as exc:
        logger.error("Failed to retrieve metadata: %s", exc)
        ctx.exit(1)

    try:
        url, result = dpd.parse_metadata(m)
    except ParseFailedError as exc:
        logger.error("Failed to parse metadata: %s", exc)
        ctx.exit(1)

    display.display(result)

    if ctx.obj["clip"]:
        clipboard(url)


@main.command("econt")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.option(
    "-u",
    "--username",
    default="default",
    help="User name to use for the API calls",
)
@click.pass_context
def cli_econt(
    ctx: click.Context,
    awb: str,
    username: str,
) -> None:
    """Retrieve tracking information from Econt <https://econt.com>.

    Given an AWB, we can retrieve basic information about a parcel's history
    and its current status. For additional information, check the provided URL.
    """
    from boxscout import econt

    if not awb:
        awb = awb_prompt("econt")

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    username, password = econt.get_credentials(username)
    if password is None:
        logger.error("Must provide a 'username' and 'password' or 'password_cmd'.")
        ctx.exit(1)

    try:
        m = econt.get_metadata(awb, username=username, password=password)
    except MetadataFailedError as exc:
        logger.error("Failed to retrieve metadata: %s", exc)
        ctx.exit(1)

    try:
        url, result = econt.parse_metadata(m)
    except ParseFailedError as exc:
        logger.error("Failed to parse metadata: %s", exc)
        ctx.exit(1)

    display.display(result)

    if ctx.obj["clip"]:
        clipboard(url)


@main.command("sameday")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.pass_context
def cli_sameday(
    ctx: click.Context,
    awb: str,
) -> None:
    """Retrieve tracking information from SAMEDAY <https://sameday.ro>.

    Given an AWB, we can retrieve basic information about a parcel's history
    and its current status. For additional information, check the provided URL.
    """
    from boxscout import sameday

    if not awb:
        awb = awb_prompt("sameday")

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    try:
        m = sameday.get_metadata(awb)
    except MetadataFailedError as exc:
        logger.error("Failed to retrieve metadata: %s", exc)
        ctx.exit(1)

    try:
        url, result = sameday.parse_metadata(m)
    except ParseFailedError as exc:
        logger.error("Failed to parse metadata: %s", exc)
        ctx.exit(1)

    display.display(result)

    if ctx.obj["clip"]:
        clipboard(url)


@main.command("fancourier")
@click.help_option("--help", "-h")
@click.argument("awb", default="")
@click.option(
    "-u",
    "--username",
    default="default",
    help="User name to use for the API calls",
)
@click.pass_context
def cli_fan_courier(
    ctx: click.Context,
    awb: str,
    username: str,
) -> None:
    """Retrieve tracking information from FAN Courier <https://fancourier.com>.

    Given an AWB, we can retrieve basic information about a parcel's history
    and its current status. For additional information, check the provided URL.
    """
    from boxscout import fancourier

    if not awb:
        awb = awb_prompt("fancourier")

    if not awb:
        logger.error("Must provide an AWB.")
        ctx.exit(1)

    username, client_id, password = fancourier.get_credentials(username)
    if password is None:
        logger.error("Must provide a 'password' or 'password_cmd'.")
        ctx.exit(1)

    if client_id is None:
        logger.error("Must provide a 'client_id'.")
        ctx.exit(1)

    try:
        m = fancourier.get_metadata(
            awb,
            username=username,
            client_id=client_id,
            password=password,
        )
    except MetadataFailedError as exc:
        logger.error("Failed to retrieve metadata: %s", exc)
        ctx.exit(1)

    try:
        url, result = fancourier.parse_metadata(m)
    except ParseFailedError as exc:
        logger.error("Failed to parse metadata: %s", exc)
        ctx.exit(1)

    display.display(result)

    if ctx.obj["clip"]:
        clipboard(url)


# }}}
