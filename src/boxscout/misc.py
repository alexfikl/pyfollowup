# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import re
import sys
from typing import Any
from xml.etree import ElementTree  # noqa: S405

import platformdirs
import requests

BOXSCOUT_CONFIG_DIR = platformdirs.user_config_path("boxscout")
BOXSCOUT_CONFIG_FILE = BOXSCOUT_CONFIG_DIR / "boxscout.toml"

BOXSCOUT_CACHE_DIR = platformdirs.user_cache_path("boxscout")
BOXSCOUT_CACHE_FILE = BOXSCOUT_CACHE_DIR / "boxscout.json"


PROVIDER_DISPLAY_NAME = {
    "gls": "GLS",
    "dpd": "DPD",
    "sameday": "SAMEDAY",
    "econt": "ECONT",
    "fancourier": "FAN Courier",
}


# {{{ logging


def make_logger(name: str) -> logging.Logger:
    import rich.logging

    root = logging.getLogger("boxscout")
    if not root.handlers:
        root.setLevel(logging.INFO)
        root.addHandler(rich.logging.RichHandler())

    return logging.getLogger(name)


# }}}


# {{{ exceptions


class MetadataFailedError(RuntimeError):
    """Exception raised when retrieving metadata from a remote source has failed."""


class ParseFailedError(RuntimeError):
    """Exception raised when parsing the metadata from a remote source has failed."""


# }}}


# {{{ configuration


def parse_config(provider: str) -> dict[str, Any]:
    if sys.version_info < (3, 11):
        import tomli as tomllib  # type: ignore[import-not-found,unused-ignore]
    else:
        import tomllib

    with open(BOXSCOUT_CONFIG_FILE, "rb") as f:
        settings = tomllib.load(f)

    return dict(settings.get(provider, {}))


def get_password_command(password_cmd: str) -> str | None:
    import shlex
    import subprocess  # noqa: S404

    password: str | None = None
    try:
        p = subprocess.Popen(  # noqa: S603
            shlex.split(password_cmd),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result, _ = p.communicate()
        password = None if p.returncode != 0 else result.decode()
    except Exception:
        password = None

    return password


# }}}


# {{{ history


def _history_ensure_cache_file() -> None:
    if not BOXSCOUT_CACHE_DIR.exists():
        BOXSCOUT_CACHE_DIR.mkdir()

    if not BOXSCOUT_CACHE_FILE.exists():
        BOXSCOUT_CACHE_FILE.touch()


def history_clear() -> None:
    """Clear the whole history."""
    BOXSCOUT_CONFIG_FILE.unlink(missing_ok=True)


def history_retrieve() -> dict[str, Any]:
    """Retrieve a mapping of providers to known AWB from the history.

    :returns: a mapping of the AWB for the given *provider* if not *None*, but
        otherwise it returns all the providers.
    """
    _history_ensure_cache_file()

    import json

    with open(BOXSCOUT_CACHE_FILE, encoding="utf-8") as f:
        try:
            providers = json.load(f)
        except json.decoder.JSONDecodeError:
            providers = {}

    if not isinstance(providers, dict):
        return {}

    return providers


def history_save(providers: dict[str, Any]) -> None:
    """Save a mapping of providers to the cache file."""
    _history_ensure_cache_file()

    import json

    with open(BOXSCOUT_CACHE_FILE, "w", encoding="utf-8") as f:
        json.dump(providers, f, indent=2, sort_keys=True)


def history_search(providers: dict[str, Any], awb: str) -> str | None:
    """Search for the given *awb* in the providers mapping.

    :returns: the provider to which the awb belongs, if any.
    """
    awb_to_provider = {
        awb: provider for provider, awbs in providers.items() for awb in awbs
    }

    return awb_to_provider.get(awb)


# }}}


# {{{ parsing


def etree_to_dict(t: ElementTree.Element) -> dict[str, Any]:
    from collections import defaultdict

    d: dict[str, Any] = {}
    children = list(t)

    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.items():
                dd[k].append(v)

        d[t.tag] = {k: v[0] if len(v) == 1 else v for k, v in dd.items()}
    else:
        d[t.tag] = {} if t.attrib else None

    if t.attrib:
        d[t.tag].update((f"@{k}", v) for k, v in t.attrib.items())

    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]["#text"] = text
        else:
            d[t.tag] = text

    return d


# }}}


# {{{ clean_string

_CAPITALIZED_WORDS = frozenset({
    "(DE)",
    "(HU)",
    "(RO)",
    "DPD",
    "ECONT",
    "GLS",
    "H&M",
    "LLC",
    "SAMEDAY",
    "SRL",
    "Easybox",
    "PIN",
    "QR",
})

CAPITALIZED_WORDS = {w.lower(): w for w in _CAPITALIZED_WORDS}
RE_SPLIT_WORDS = re.compile(r"\s+")
RE_FIRST_WORD = re.compile(r"((?<=[\.\?!;-]\s)(\w+)|(^\w+))")


def capitalize_word(m: re.Match[Any]) -> str:
    return str(m.group()).capitalize()


def title_string(text: str) -> str:
    words = RE_SPLIT_WORDS.split(text.title().strip("."))

    return " ".join(CAPITALIZED_WORDS.get(w.lower(), w) for w in words)


def clean_string(text: str) -> str:
    # capitalize first word of sentences
    result = RE_FIRST_WORD.sub(capitalize_word, text.lower())
    # uppercase words
    words = RE_SPLIT_WORDS.split(text.strip("."))
    result = " ".join(CAPITALIZED_WORDS.get(w.lower(), w) for w in words)

    return result


# }}}


# {{{ get_metadata


def query_api(api: str, mode: str = "GET", **kwargs: Any) -> requests.Response:
    try:
        if mode == "GET":
            response = requests.get(api, timeout=5, **kwargs)
        else:
            response = requests.post(api, timeout=5, **kwargs)
    except requests.exceptions.ConnectTimeout as exc:
        raise MetadataFailedError("Connection timed out on API call") from exc

    if not response.ok:
        raise MetadataFailedError(
            f"Failed to metadata: {response.reason} ({response.status_code})"
        )

    return response


def get_text_metadata(api: str, mode: str = "GET", **kwargs: Any) -> str:
    response = query_api(api, mode=mode, **kwargs)
    return response.text


def get_json_metadata(api: str, mode: str = "GET", **kwargs: Any) -> dict[str, Any]:
    response = query_api(api, mode=mode, **kwargs)

    try:
        return dict(response.json())
    except requests.exceptions.JSONDecodeError as exc:
        raise MetadataFailedError(
            f"Failed to decode the JSON response (check your AWB): {response.text}"
        ) from exc


# }}}
