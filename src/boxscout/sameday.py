# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime
from typing import Any

from boxscout import display
from boxscout.misc import ParseFailedError, clean_string, make_logger

logger = make_logger(__name__)

SAMEDAY_URL = "https://sameday.ro/#awb={awb}"
SAMEDAY_API_RUL = "https://api.sameday.ro/api/public/awb/{awb}/awb-history?_locale=en"


def get_metadata(awb: str) -> dict[str, Any]:
    from boxscout.misc import get_json_metadata

    api = SAMEDAY_API_RUL.format(awb=awb)
    return get_json_metadata(api)


def parse_metadata(metadata: dict[str, Any]) -> tuple[str, display.ParseResult]:
    result = metadata.get("awbHistory")
    if result is None:
        raise ParseFailedError("Response has unexpected format")

    # get back awb
    awb = metadata["awbNumber"]

    # web tracking
    url = SAMEDAY_URL.format(awb=awb)
    web = display.Section(
        name="Web Tracking",
        children=(display.Info("URL", url),),
    )

    # info
    info = display.Section(
        name="Shipment Info",
        children=(
            display.Info("Service", "SAMEDAY"),
            display.Info("Parcel number", awb),
            display.Info("Return", str(bool(metadata["isReturn"]))),
        ),
    )

    # status
    children = []
    for event in result:
        descr = event["status"]
        date = datetime.fromisoformat(event["statusDate"])

        county = event.get("county")
        if county:
            location = "{} ({})".format(county, event["country"])
        else:
            location = "{}".format(event["country"])

        location = location.strip()
        if location == "()":
            location = "SAMEDAY center"

        children.append(
            display.Event(
                code=0,
                date=date,
                location=clean_string(location),
                descr=clean_string(descr),
            )
        )
    status = display.Section(name="Shipment Status", children=tuple(children))

    return url, display.ParseResult(web, info, status)
