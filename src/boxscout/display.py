# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass
from datetime import datetime
from typing import IO, Any, Generic, NamedTuple, TypeVar

import click

from boxscout.misc import make_logger

logger = make_logger(__name__)


# {{{ nodes


class Node:
    """A base node class."""


NodeT = TypeVar("NodeT", bound=Node)


@dataclass(frozen=True)
class Section(Node, Generic[NodeT]):
    """A section of information to display."""

    #: A name for the section.
    name: str
    #: A list of child items in the section.
    children: tuple[NodeT, ...]


@dataclass(frozen=True)
class Event(Node):
    """An event in the parcel history."""

    #: A specific code for the event that, which can be used to influence the
    #: display of the event (e.g. an error code can be shown in red).
    code: int

    #: The date the event happened.
    date: datetime
    #: The location at which the event happened.
    location: str
    #: A short description of the event.
    descr: str


@dataclass(frozen=True)
class Info(Node):
    """An information node."""

    #: A name for the piece of information reported.
    name: str
    #: A description for the piece of information.
    descr: str | datetime


@dataclass(frozen=True)
class History(Node):
    """A history node."""

    #: A parcel number for the item.
    awb: str
    #: A description of the item.
    note: str
    #: A flag that marks the item as delivered.
    delivered: bool


# }}}


# {{{ display

INDENT_WIDTH = 4


class Indent:
    def __init__(self, d: Display) -> None:
        self.d = d

    def __enter__(self) -> Display:
        self.d.indent += INDENT_WIDTH
        return self.d

    def __exit__(self, *args: Any, **kwargs: Any) -> None:
        self.d.indent -= INDENT_WIDTH


class Display:
    def __init__(
        self, *, file: IO[Any] | None = None, color: bool | None = None
    ) -> None:
        self.file = file
        self.color = color
        self.indent = 0

    @classmethod
    def format_date(cls, d: datetime) -> str:
        return d.strftime("%d.%m.%Y %H:%M")

    def echo(self, messages: Iterable[str], *, bold: bool | None = None) -> None:
        click.echo(
            " " * self.indent + "".join(messages),
            file=self.file,
            color=self.color,
        )

    def __call__(self, nodes: Node | list[Node] | tuple[Node, ...]) -> None:
        self.indent = 0
        self.rec(nodes)

    def rec(self, nodes: Node | list[Node] | tuple[Node, ...]) -> None:
        if isinstance(nodes, Node):
            try:
                method = getattr(self, f"show_{type(nodes).__name__}")
            except AttributeError:
                raise TypeError(
                    f"Cannot display nodes of type '{type(nodes).__name__}'"
                ) from None

            method(nodes)
        elif isinstance(nodes, tuple | list):
            for node in nodes:
                self.rec(node)
        else:
            raise TypeError(
                f"Cannot display nodes of type '{type(nodes).__name__}'"
            ) from None

    def show_Section(self, section: Section[Any]) -> None:
        self.echo([click.style(section.name.upper(), bold=True)])
        with Indent(self):
            for node in section.children:
                self.rec(node)

    def show_Event(self, event: Event) -> None:
        self.echo([
            "" if event.code == 0 else click.style("\b\b! ", fg="red", bold=True),
            f"({self.format_date(event.date)})",
            " [",
            click.style(event.location, fg="yellow"),
            "] ",
            click.style(
                f"{event.descr}.",
                fg="green" if event.code == 0 else "red",
                bold=event.code != 0,
            ),
        ])

    def show_Info(self, info: Info) -> None:
        descr = (
            self.format_date(info.descr)
            if isinstance(info.descr, datetime)
            else info.descr
        )
        self.echo([info.name, ": ", click.style(descr, fg="green")])

    def show_History(self, hist: History) -> None:
        self.echo([
            click.style("\b\b✓ " if hist.delivered else "", bold=True),
            hist.awb,
            ": ",
            click.style(hist.note, fg="white" if hist.delivered else "green"),
        ])


def display(
    message: Node | list[Node] | tuple[Node, ...], file: IO[Any] | None = None
) -> None:
    """Display the given nodes.

    :arg file: a file to write the contents of the nodes, which defaults to STDOUT.
    """
    Display(file=file)(message)


def stringify(
    message: Node | list[Node] | tuple[Node, ...], *, color: bool | None = None
) -> str:
    """Convert the given nodes to a string representation."""
    import io

    output = io.StringIO()
    Display(file=output, color=color)(message)

    result = output.getvalue()
    output.close()

    return result.strip()


# }}}


# {{{ parsing


class ParseResult(NamedTuple):
    #: Web tracking formation for a parcel.
    web: Section[Info]
    #: General tracking information for the parcel, e.g. destination.
    info: Section[Info]
    #: History of the parcel at different checkpoints, as available.
    history: Section[Event]


# }}}
