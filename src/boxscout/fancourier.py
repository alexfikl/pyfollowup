# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import enum
from datetime import datetime
from typing import Any

from boxscout import display
from boxscout.misc import (
    PROVIDER_DISPLAY_NAME,
    ParseFailedError,
    clean_string,
    make_logger,
)

logger = make_logger(__name__)

# https://pdfcoffee.com/api-integrare-selfawb-pdf-free.html
FAN_COURIER_API_URL = "https://www.selfawb.ro/awb_tracking_integrat.php"
FAN_COURIER_URL = (
    "https://www.fancourier.ro/awb-tracking/?metoda=tracking&bar_size=x&awb={awb}"
)
FAN_COURIER_DISPLAY_NAME = PROVIDER_DISPLAY_NAME["fancourier"]


@enum.unique
class DisplayMode(enum.IntEnum):
    LastStatus = 1
    LastHistory = 2
    FullHistory = 3
    ReceivedConfirmation = 4
    JSON = 5


def get_credentials(user: str) -> tuple[str, str | None, str | None]:
    from boxscout.misc import get_password_command, parse_config

    settings = parse_config("fancourier")
    username = settings.get(user, settings).get("username", user)
    client_id = settings.get(user, settings).get("client_id")
    password = settings.get(user, settings).get("password")

    if password is None:
        password_cmd = settings.get(username, settings).get("password_cmd")
        if password_cmd is not None:
            password = get_password_command(password_cmd)

    return username, client_id, password


def get_metadata(
    awb: str,
    *,
    username: str,
    client_id: str,
    password: str,
) -> dict[str, Any]:
    from boxscout.misc import get_json_metadata, get_text_metadata

    data = {
        "AWB": awb,
        "display_mode": DisplayMode.JSON.value,
        "username": username,
        "client_id": client_id,
        "user_pass": password,
        "language": "en",
    }

    result = get_json_metadata(FAN_COURIER_API_URL, mode="POST", data=data)
    if result.get("progressdetail") is None:
        # NOTE: this can happen even for valid AWBs if they were just entered
        # into the system. In that case FullHistory gives a nicer blurb
        data["display_mode"] = DisplayMode.FullHistory.value
        text = get_text_metadata(FAN_COURIER_API_URL, mode="POST", data=data)

        if "," in text:
            _, text = text.split(",")

        today = datetime.today()
        result = {
            "status": "unknown",
            "progressdetail": [
                {
                    "activity": text.strip(),
                    "deliverylocation": f"{FAN_COURIER_DISPLAY_NAME} center",
                    "deliverydate": today.strftime("%d.%m.%Y"),
                    "deliverytime": today.strftime("%H:%M"),
                }
            ],
        }

    result["awbNumber"] = awb
    return result


def parse_metadata(metadata: dict[str, Any]) -> tuple[str, display.ParseResult]:
    result = metadata.get("progressdetail")
    if result is None:
        raise ParseFailedError("Response has unexpected format")

    awb = metadata["awbNumber"]

    # web tracking
    url = FAN_COURIER_URL.format(awb=awb)
    web = display.Section(
        name="Web Tracking",
        children=(display.Info("URL", url),),
    )

    # info
    infos = [
        display.Info("Service", FAN_COURIER_DISPLAY_NAME),
        display.Info("Parcel number", awb),
    ]
    if metadata["status"].lower() == "delivered":
        delivery_date = datetime.strptime(
            "{} {}".format(metadata["deliverydate"], metadata["deliverytime"]),
            "%d.%m.%Y %H:%M",
        )
        infos.extend([
            display.Info("Delivery date", delivery_date),
            display.Info(
                "Receiver", metadata["activity"].split(":")[-1].strip().title()
            ),
        ])
    info = display.Section(name="Shipment Info", children=tuple(infos))

    # status
    events = []
    for event in result:
        date = datetime.strptime(
            "{} {}".format(event["deliverydate"], event["deliverytime"]),
            "%d.%m.%Y %H:%M",
        )
        location = event["deliverylocation"]
        descr = event["activity"]

        events.append(
            display.Event(
                code=0,
                date=date,
                location=clean_string(location),
                descr=clean_string(descr),
            )
        )
    status = display.Section(name="Shipment Status", children=tuple(events))

    return url, display.ParseResult(web, info, status)
