# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime
from typing import Any
from xml.etree import ElementTree  # noqa: S405

import requests

from boxscout import display
from boxscout.misc import MetadataFailedError, ParseFailedError, make_logger

logger = make_logger(__name__)

ECONT_URL = "https://www.econt.com/en/services/track-shipment/{awb}"
ECONT_SERVICE_URL = "http://www.econt.com/e-econt/xml_service_tool.php"
ECONT_REQUEST_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8"?>
<request>
    <client>
        <username>{username}</username>
        <password>{password}</password>
    </client>
    <request_type>{request_type}</request_type>
    <updated_time>{updated_time}</updated_time>
    <shipments full_tracking="ON">
        <num>{number}</num>
    </shipments>
    <client_software>{client_software}</client_software>
</request>"""

ECONT_EVENT_DESCRIPTION = {
    "client": "Delivered",
    "imported": "Accepted in ECONT",
    "info": "Information",
    "office": "Arrival at an office",
    "prepared": "Awaiting delivery to ECONT",
}

ECONT_TRANSLATIONS = {
    "(В куриер)": " (by courier)",  # noqa: RUF001
}


def get_credentials(user: str) -> tuple[str, str | None]:
    """
    :returns: a tuple of ``(username, password)``. By default, the user name is
        taken to be *user*, but the password must be provided in the configuration
        file.
    """
    from boxscout.misc import get_password_command, parse_config

    settings = parse_config("econt")
    username = settings.get(user, settings).get("username", user)
    password = settings.get(user, settings).get("password")
    if password is None:
        password_cmd = settings.get(username, settings).get("password_cmd")
        if password_cmd is not None:
            password = get_password_command(password_cmd)

    return username, password


def get_metadata(awb: str, *, username: str, password: str) -> dict[str, Any]:
    data = ECONT_REQUEST_TEMPLATE.format(
        username=username,
        password=password,
        request_type="shipments",
        updated_time="",
        number=awb,
        client_software="econt-track.py",
    )

    try:
        response = requests.post(ECONT_SERVICE_URL, files={"file": data}, timeout=5)
    except requests.exceptions.ConnectTimeout as exc:
        raise MetadataFailedError("Connection timed out on API call") from exc

    if not response.ok:
        raise MetadataFailedError(
            f"Failed to metadata: {response.reason} ({response.status_code})"
        )

    from boxscout.misc import etree_to_dict

    root = ElementTree.fromstring(response.text)  # noqa: S314
    return etree_to_dict(root)


def parse_metadata(metadata: dict[str, Any]) -> tuple[str, display.ParseResult]:
    result = metadata.get("response", {}).get("shipments", {}).get("e")
    if result is None:
        raise ParseFailedError("Response has unexpected format")

    from boxscout.misc import clean_string, title_string

    awb = result["loading_num"]

    # web tracking
    url = ECONT_URL.format(awb=awb)
    web = display.Section(
        name="Web Tracking",
        children=(display.Info("URL", url),),
    )

    # info
    creation_date = datetime.strptime(result["created_time"], "%m/%d/%Y %H:%M:%S")
    delivery_date = datetime.strptime(result["expected_delivery_day"], "%Y-%m-%d")
    info = display.Section(
        name="Shipment Info",
        children=(
            display.Info("Service", "ECONT"),
            display.Info("AWB", awb),
            display.Info("Sender", title_string(result["sender_name"])),
            display.Info("Creation date", creation_date),
            display.Info("Delivery date", delivery_date),
            display.Info("Status", clean_string(result["short_delivery_status"])),
        ),
    )

    # status
    events = result["tracking"]["row"]
    if not isinstance(events, list):
        events = [events]

    children = []
    country = "Romania"
    for event in events:
        date = datetime.strptime(event["time"], "%d.%m.%Y %H:%M")

        country = event.get("evn_country_name_en") or country
        city = event["evn_city_name_en"]
        location = f"{city}, {country}".format(city, country) if city else country

        event_type = event["event"]
        descr = ECONT_EVENT_DESCRIPTION.get(event_type, event.get("name_en"))
        if descr is None:
            descr = f"Unknown event type: '{event_type}'"

        if event["event"] == "info":
            name = event["name"]
            for bg, en in ECONT_TRANSLATIONS.items():
                name = name.replace(bg, en)

            descr = f"{descr} - {name}"

        children.append(
            display.Event(
                code=0,
                date=date,
                location=clean_string(location),
                descr=clean_string(descr),
            )
        )
    status = display.Section(name="Shipment Status", children=tuple(children))

    return url, display.ParseResult(web, info, status)
