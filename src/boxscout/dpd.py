# SPDX-FileCopyrightText: 2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from datetime import datetime
from typing import Any

from boxscout import display
from boxscout.misc import ParseFailedError, clean_string, make_logger

logger = make_logger(__name__)

DPD_URL = "https://tracking.dpd.de/status/en_RO/parcel/{awb}"
DPD_API_URL = "https://tracking.dpd.de/rest/plc/en_DE/{awb}"


def get_metadata(awb: str) -> dict[str, Any]:
    from boxscout.misc import get_json_metadata

    api = DPD_API_URL.format(awb=awb)
    return get_json_metadata(api)


def parse_metadata(metadata: dict[str, Any]) -> tuple[str, display.ParseResult]:
    result = metadata.get("parcellifecycleResponse", {}).get("parcelLifeCycleData")
    if result is None:
        raise ParseFailedError("Response has unexpected format")

    # get back AWB
    awb = result["shipmentInfo"]["parcelLabelNumber"]

    # web tracking
    url = DPD_URL.format(awb=awb)
    web = display.Section(
        name="Web Tracking",
        children=(display.Info("URL", url),),
    )

    # info
    info = display.Section(
        name="Shipment Info",
        children=(
            display.Info("Service", "DPD"),
            display.Info("Parcel number", awb),
            display.Info("Type", result["shipmentInfo"]["productName"]),
        ),
    )

    # status
    children = []
    for event in result["scanInfo"]["scan"]:
        date = datetime.fromisoformat(event["date"])
        location = event["scanData"]["location"]
        descr = event["scanDescription"]["content"][0].strip(".")

        children.append(
            display.Event(
                code=0,
                date=date,
                location=clean_string(location),
                descr=clean_string(descr),
            )
        )
    status = display.Section(name="Shipment Status", children=tuple(children))

    return url, display.ParseResult(web, info, status)
